var info =
    {
        "name": "Paweł",
        "surname": "Wieraszka",
        "adress1" : "Reformacka 19A/7",
        "adress2" : "80-808, Gdańsk"
    };

var educationS =  [
    {
        "school": "VIII L.O. in Gdansk",
        "subject": "extended mathematics and physics",
        "years": "1999 - 2003",
        "title": "A levels"

    },
    {
        "school": "Gdansk University of Technology",
        "subject": "Electrical engineering",
        "years": "2003 - 2008",
        "title": "Master of Science (2008)"
    },
    {
        "school": "Gdansk University of Technology",
        "subject": "Management and Economics",
        "years": "2007 - 2010",
        "title": "Master (2013)"
    },
    {
        "school": "Codementors",
        "subject": "Java Junior Developer",
        "years": "II.2018 - VI.2018",
        "title": ""
    }

];

var experienceS =
    [
        {
            "company": "SPIE Controlec Engineering",
            "adress": "Schiedam (Netherlands)",
            "years": "V.2010 - VIII.2013",
            "position": "Junior Engineer"
        },
        {
            "company": "Kalkuluj.pl Sp. z o.o.",
            "adress": "Gdansk",
            "years": "IX.2013 - now",
            "position": "Worker / Co-owner"
        }
    ];

var it_skills = [
    "Java", "JavaFX", "Maven", "Hibernate", "Java Persistence API", "SQL + MySQL", "NoSQL + MongoDB",
    "HTML", "CSS", "JavaScript", "jQuery", "JSON", "Angular", "Microservices", "Linux", "Git"
];

var lan_skills = [
    "English - Advanced", "Dutch - Advanced", "German - Basic"
];

var interests = [
    "speedway", "football", "sports overall", "astrology", "Rubik's Cube"
];

function populateCv(container) {

    var container = "#" + container;
    var article = $("<article></article>");
    $(container).append(article);

    article.append(createHeader('<h2>','cv'));

    article.append($("<section></section>")
        .append(createInfoTable('Basic information', info)));

    article.append($("<section></section>")
        .append(createEduTable('education', 'school', 'subject', 'title', 'years')));

    article.append($("<section></section>")
        .append(createExpTable('experience', 'company', 'adress', 'position', 'years')));

    article.append($("<section></section>")
        .append(createHeader('<h3>', 'it skills'))
        .append(createList(it_skills, 'itSkills')));

    article.append($("<section></section>")
        .append(createHeader('<h3>', 'language skills'))
        .append(createList(lan_skills, 'lanSkills')));

    article.append($("<section></section>")
        .append(createHeader('<h3>', 'interests'))
        .append(createList(interests, 'intSkills')));
}

function createHeader(name, text) {
    return $("<header></header>").append($(name).text(text));
}

function createList(skills, class_name) {
    var ul = $("<ul></ul>").addClass(class_name);

    for (var index in skills) {
        var skill = skills[index];
        ul.append($("<li></li>").text(skill));
    }
    return ul;
}

function createExpTable(name, v1, v2, v3, v4) {
    return $("<table></table>").addClass("Table").append($("<th></th>").text(name).attr("colspan", name.length))
        .append(createExpRow(v1)).append(createExpRow(v2))
        .append(createExpRow(v3)).append(createExpRow(v4));
}

function createExpRow(name) {
    var tr = $("<tr></tr>");

    for (var index in experienceS) {
        var experience = experienceS[index];
        tr.append($("<td></td>").text(experience[name]));
    }

    return tr;
}

function createEduTable(name, v1, v2, v3, v4) {
    return $("<table></table>").addClass("Table").append($("<th></th>")
        .text(name).attr("colspan", name.length)).append(createEduRow(v1))
        .append(createEduRow(v2)).append(createEduRow(v3)).append(createEduRow(v4));
}

function createEduRow(name) {
    var tr = $("<tr></tr>");

    for (var index in educationS) {
        var education = educationS[index];
        tr.append($("<td></td>").text(education[name]));
    }

    return tr;
}

function createInfoTable(name, info) {
    return $("<table></table>").addClass("Table").append($("<th></th>").attr("colspan", 2).text(name))
        .append(createOneRow("Name:", info.name)).append(createOneRow("Surname:", info.surname))
        .append(createOneRow("Adress:", info.adress1)).append(createOneRow("", info.adress2));
}

function createOneRow(header, value) {
    return $("<tr></tr>").append($("<td></td>").text(header)).append($("<td></td>").text(value));
}